﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarjutusiKordamiseks
{
    class Program
    {
 
        static void Main(string[] args)
        {
            Console.Write("Tere tulemast kordamisharjutust tegema\nvali harjutuse number (näidiseks 0): ");
            int harjutuseNumber;
            while (!int.TryParse(Console.ReadLine(), out harjutuseNumber))
            {
                Console.Write("Palun korralik number:");
            }
            if (harjutuseNumber >= 0)
            {
                Console.WriteLine($"valisid harjutuse {harjutuseNumber}");
                Ettevalmistus(harjutuseNumber);
                switch (harjutuseNumber)
                {
                    case 0:
                        // harjutus 0 - tänase nädalapäeva ja kõige suurema täisarvu leidmine
                        // tee muutuja, vali ise õige tüüp ja anna talle väärtuseks suurim int-arv
                        // tee teine muutuja, vali ise õige tüüp ja pane väärtuseks tänane nädalapäev
                        // trüki kõik leitud asjad ekraanile - vaata, kas toimib (ctrl-F5)
                        // 
                        // TODO: 1. kirjuta esimese harjutuse lahendus selle ja END 1 rea vahele

                        //lahendus:

                        Console.WriteLine($"{harjutuseNumber}. harjutuse lahendus");

                        string[] nädalapäevaNimed = System.Globalization.CultureInfo
                            .CurrentCulture.DateTimeFormat.DayNames;
                        int suurim = int.MaxValue;
                        DayOfWeek täna = DateTime.Now.DayOfWeek;
                        Console.WriteLine($"suurim täisarv on {suurim} ja täna on {täna}");
                        Console.WriteLine($"täna on {  nädalapäevaNimed[ (int)täna ]   }");

                        Console.WriteLine($"täna on {DateTime.Now:dddd}");
 
                        // END 1
                        break;
                    case 1:
                        // harjutus 1 - massiivis olevate arvude liitmine ja kokkulugemine
                        // sul on täisarvumassiiv int[] nimega Arvud;
                        // seal on läbisegi positiivsed (>0) ja negatiivsed (<0) arvud, mõned on ka nullid (==0)
                        // loe kokku kui palju on positiivseid, negatiivseid ja nulle
                        // leia positiivsete arvude summa ja keskmine
                        // leia negatiivsete arvude summa ja keskmine
                        // leia arvude kogusumma
                        // trüki kõik leitud asjad ekraanile - vaata, kas toimib (ctrl-F5)
                        // 
                        // TODO: 1. kirjuta esimese harjutuse lahendus selle ja END 1 rea vahele

                        int posSumma = 0;
                        int negSumma = 0;
                        int posCount = 0;
                        int negCount = 0;
                        int nulCount = 0;

                        // emb kumb nendest tsüklitest

                        //for(int i = 0; i < Arvud.Length; i++)
                        //{
                        //    if (Arvud[i] > 0) { posSumma += Arvud[i]; posCount++; }
                        //    else if (Arvud[i] < 0) { negSumma += Arvud[i]; negCount++; }
                        //    else { nulCount++; }
                        //}

                        foreach (int x in Arvud)
                        {
                            if (x > 0) { posSumma += x; posCount++; }
                            else if (x < 0) { negSumma += x; negCount++; }
                            else nulCount++;
                        }


                        Console.WriteLine($"positiivseid on {posCount} ja nende summa on {posSumma}");
                        Console.WriteLine($"negatiivseid on {negCount} ja nende summa on {negSumma}");
                        Console.WriteLine($"nulle on {nulCount}");
                        Console.WriteLine($"kokku on {Arvud.Length} ja summa on {posSumma + negSumma}");
                        Console.WriteLine($"positiivsete keskmine on { (double)posSumma / posCount }");
                        Console.WriteLine($"negatiivsete keskmine on { (double)negSumma / negCount }");

                        // END 1 -- tehtud!!!
                        break;
                    case 2:
                        // harjutus 2 - massiivist kahe uue tegemine
                        // sul on täisarvumassiiv int[] nimega Arvud; (nagu eelmises harjutuses)
                        // tee sellest kaks uut massiivi positiivsedArvud ja negatiivsedArvud,
                        // mis sisaldavavad vastavalt positiivseid ja negatiivseid 
                        // ja mis oleks paraja suurusega
                        // trüki ekraanile ühe reana kõik positiivsed ja teise reana kõik negatiivsed
                        // vaata kas toimib (ctrl-F5)
                        //
                        // TODO: 2. kirjuta teise harjutuse lahendus selle ja END 2 rea vahele

                        // mida oleks meil vaja 2. harjutuse lahendamiseks?
                        List<int> positiivsed = new List<int>(Arvud.Length);
                        List<int> negatiivsed = new List<int>(Arvud.Length);
                        foreach (var x in Arvud)
                            if (x > 0) positiivsed.Add(x);
                            else if (x < 0) negatiivsed.Add(x);


                        int[] positiivsedArvud = new int[Arvud.Count(x => x > 0)];
                        int[] negatiivsedArvud = new int[Arvud.Count(x => x < 0)];
                        int posCount2 = 0;
                        int negCount2 = 0;
                        foreach (var x in Arvud)
                            if (x > 0) positiivsedArvud[posCount2++] = x;
                            else if (x < 0) negatiivsedArvud[negCount2++] = x;
                        //Array.Resize(ref positiivsedArvud, posCount2);
                        //Array.Resize(ref negatiivsedArvud, negCount2);

                        // ei hakka tegema massiiviks, kuna List on sama hea (sisemiselt massiiv)
                        //int[] posTulemus = positiivsed.ToArray();
                        //int[] negTulemus = negatiivsed.ToArray();

                        Console.WriteLine(string.Join(" ", positiivsed));

                        foreach (var x in negatiivsed) Console.Write($"{x} ");
                        Console.WriteLine();

                        Console.WriteLine(string.Join(", ", positiivsedArvud));
                        Console.WriteLine(string.Join(", ", negatiivsedArvud));

                        // END 2
                        break;
                    case 3:
                        // harjutus 3 - sul on kaks nimekirja List<string> Nimed ja TeisedNimed
                        // tee uus nimekiri (uus muutuja), kus on vaid need, kes on mõlemas nimekirjas
                        // trüki see nimekiri välja
                        // vaata kas toimib (ctrl-F5)
                        //
                        // TODO: 3. kirjuta kolmanda harjutuse lahendus selle ja END 3 rea vahele
                        Console.WriteLine($"{harjutuseNumber}. harjutuse lahendus");

                        List<string> nimekiri3 = new List<string>();

                        foreach (var x in Nimed)
                        {
                            if (TeisedNimed.Contains(x)) nimekiri3.Add(x);
                        }

                        // List<string> nimekiri3 = Nimed.Intersect(TeisedNimed).ToList();

                        Console.WriteLine(string.Join(", ", nimekiri3));

                        // END 3
                        break;
                    case 4:
                        // harjutus 4 - sul on kaks nimekirja List<string> Nimed ja TeisedNimed
                        // tee uus nimekiri (uus muutuja), kus on vaid need, kes on emmas kummas, 
                        // aga mitte mõlemas nimekirjas
                        // trüki see nimekiri välja
                        // vaata kas toimib (ctrl-F5)
                        //
                        // TODO: 4. kirjuta neljanda harjutuse lahendus selle ja END 4 rea vahele
                        Console.WriteLine($"{harjutuseNumber}. harjutuse lahendus");

                        List<string> nimekiri4 = new List<string>();
                        foreach (var x in Nimed) if (!TeisedNimed.Contains(x)) nimekiri4.Add(x);
                        foreach (var x in TeisedNimed) if (!Nimed.Contains(x)) nimekiri4.Add(x);

                        //List<string> nimekiri4a = new List<string>();
                        //foreach (var x in Nimed.Union(TeisedNimed))
                        //    if (!Nimed.Intersect(TeisedNimed).Contains(x)) nimekiri4a.Add(x);

                        List<string> nimekiri4a =
                            Nimed
                            .Union(TeisedNimed)
                            .Except(Nimed.Intersect(TeisedNimed))
                            .ToList();

                        Console.WriteLine(string.Join(", ", nimekiri4));
                        Console.WriteLine(string.Join(", ", nimekiri4a));

                        // END xx
                        break;
                    case 5:
                        // harjutus 5 - sul on nimekiri (List<Inimene>) Inimesed
                        // igal inimesel on .Nimi, .Vanus, .Sünniaeg
                        // 5.1 leia keskmine vanus ja trüki see välja
                        // 5.2 leia, kelle sünnipäev tuleb järgmisena ja trüki see välja
                        // 5.3 leia, kas on keegi alaealine (<18) ja mitu päeva ta veel alaealine on
                        // trüki välja leitud alaealis(t)e nimed ja päevade arv, mis jäänud
                        // vaata kas toimib (ctrl-F5)

                        //
                        // TODO: 5. kirjuta viienda harjutuse lahendus selle ja END 5 rea vahele
                        Console.WriteLine($"{harjutuseNumber}. harjutuse lahendus");
                        foreach (var x in Inimesed) Console.WriteLine(x);

                        double keskmineVanus = 0;
                        foreach (Inimene i in Inimesed) keskmineVanus += i.Vanus;
                        Console.WriteLine($"Keskmine vanus on {keskmineVanus / Inimesed.Count}");

                        // kuidas leida, millal on minu JÄRGMINE sünnipäev
                        // mida me inimese kohta teame?
                        //       sünniaeg, vanus

                        foreach (var x in Inimesed)
                        {
                            Console.WriteLine($"{x.Nimi} järgmine sünnipäev on {x.Sünniaeg.AddYears(x.Vanus + 1)}");
                        }

                        int sünnipäevani = 400;

                        foreach(var x in Inimesed)
                        {
                            int sellepäevani = (x.Sünniaeg.AddYears(x.Vanus + 1) - DateTime.Today).Days;
                            if (sellepäevani < sünnipäevani) sünnipäevani = sellepäevani; 
                        }

                        //DateTime nextBirthDay = DateTime.Today.AddYears(1);
                        //foreach(var x in Inimesed)
                        //{
                        //   

                        // NB! Selle koha peal tuleb viga, kui inimene on sündinud 29.2 ja tänavu ei ole liigaasta

                        //      DateTime sünnipäev = new DateTime(DateTime.Now.Year, x.Sünniaeg.Month, x.Sünniaeg.Day);
                        //    if (sünnipäev < DateTime.Today) sünnipäev = sünnipäev.AddYears(1);
                        //    if (sünnipäev < nextBirthDay) nextBirthDay = sünnipäev;
                        //}
                        //Console.WriteLine($"järgmine sünnipäev on meil {nextBirthDay:dd.MMMM}");
                        //Console.WriteLine("sel päeval on meil sündinud:");
                        //foreach(var x in Inimesed)
                        //    if (x.Sünniaeg.Month == nextBirthDay.Month && x.Sünniaeg.Day == nextBirthDay.Day)
                        //        Console.WriteLine(x.Nimi);


                        Console.WriteLine($" järgmise sünnipäevani on {sünnipäevani} päeva");
                        Console.WriteLine("järgmine sünnipäevalaps (sünnipäevalapsed) on:");

                        foreach(var x in Inimesed)
                        {
                            int sellepäevani = (x.Sünniaeg.AddYears(x.Vanus + 1) - DateTime.Today).Days;
                            if (sellepäevani == sünnipäevani) Console.WriteLine(x);
                        }

                        Console.WriteLine("meie alaealised on:");
                        // leiad, kes on alaealine
                        // arvutad, mitu päeva jääb tema 18.a sünnipäevani
                        // trükid välja nime ja päevade arvu

                        foreach (var x in Inimesed)
                        {
                            if (x.Vanus < 18)
                            {
                                Console.WriteLine(x.Nimi);
                                DateTime saab18 = x.Sünniaeg.AddYears(18);
                                var sinnaniOn = (saab18 - DateTime.Today).Days;

                                Console.WriteLine("{0} jääb oodata {1} päeva", x.Nimi, sinnaniOn);
                            }
                        }

                        List<DateTime> sünnipäevad = new List<DateTime>();
                        foreach (var x in Inimesed) sünnipäevad.Add(x.Sünniaeg.AddYears(x.Vanus));
                        sünnipäevad.Sort();
                        sünnipäevad.Add(sünnipäevad[0].AddYears(1));
                        Console.WriteLine("sünnipäevade list:");
                        foreach (var x in sünnipäevad) Console.WriteLine(x);

                        List<int> vahemikud = new List<int>();
                        // kuidas see list täita???

                        // Sul on sorteeritud sünnipäevade list
                        // kuidas tekitada see vahemike list

                        // sünnipäevade list tuleks läbi käia - kas for või foreach
                        // kahe naabersünnipäeva vahe tuleks arvutada (kuidas) ja vahemike listi pista

                        for (int i = 1; i < sünnipäevad.Count; i++)
                            vahemikud.Add(
                            (sünnipäevad[i] - sünnipäevad[i - 1]).Days
                            );

                        Console.WriteLine("vahemike list:");
                        foreach (var x in vahemikud) Console.WriteLine(x);

                        int min = vahemikud.Min(); // praegusel juhul 7

                        //nüüd tuleks leida, kus see 7 seal listis on
                        // st mitmendal kohal

                        int mitmes = -1;
                        for (int i = 0; i < vahemikud.Count; i++)
                        {
                            if (vahemikud[i] == min)
                            {
                                mitmes = i;
                                break;
                            }
                        }

                        Console.WriteLine("sobivad sünnipäevad");
                        Console.WriteLine(sünnipäevad[mitmes]);
                        Console.WriteLine(sünnipäevad[mitmes+1]);


                        foreach (var x in Inimesed)
                            {
                                if (x.Vanus < 18)
                                {
                                    int päeviTäisealiseni =
                                        (x.Sünniaeg.AddYears(18) - DateTime.Today)
                                        .Days;
                                    Console.WriteLine($"{x.Nimi} saab täisealiseks {päeviTäisealiseni} päeva pärast");


                                    int aastaidTäisealiseni = päeviTäisealiseni / 365;
                                    päeviTäisealiseni = päeviTäisealiseni % 365;

                                    int kuidTäisealiseni = päeviTäisealiseni / 30;
                                    päeviTäisealiseni = päeviTäisealiseni % 30;
                                    Console.WriteLine(x.Nimi + " saab täisealiseks "
                                        + (aastaidTäisealiseni == 0 ? "" : aastaidTäisealiseni.ToString() + " aasta ")
                                        + (kuidTäisealiseni == 0 ? "" : kuidTäisealiseni.ToString() + " kuu ")
                                        + (päeviTäisealiseni == 0 ? "" : päeviTäisealiseni.ToString() + " päeva ")
                                        + "pärast"

                                        );

                                }
                            }


                        // END 5
                        break;
                    case 6:
                        // harjutus 6 - sul on pakk kviitungeid
                        // kviitungid leiab kollektsioonist Kviitung.Kviitungid.Values
                        // uuri, mis omadused (osad) on kviitungil)
                        // leia ja trüki välja:
                        // * kes on ostnud kõige rohkem asju (tükke) ja kui palju
                        // * kes on ostnud kõige suurema summa eest ja see summa
                        // * millist toodet on müüdud kõige rohkem (tükke) ja kui palju
                        // * millist toodet on müüdud kõige suurema summa eest ja see summa
                        // vaata kas toimib (ctrl-F5)
                        //
                        // TODO: 6. kirjuta kuuenda harjutuse lahendus selle ja END 6 rea vahele
                        Console.WriteLine("Olemasolevad kviitungid");
                        foreach (var x in Kviitung.Kviitungid.Values) Console.WriteLine(x);
                        Console.WriteLine($"{harjutuseNumber}. harjutuse lahendus");

                        // vali variant või proovi kõiki:
                        //  proovi teha tsükliga üle kviitungite

                        Console.WriteLine("\nsee on tsükliga variant\n");
                        Dictionary<string, int> kesMituDict = new Dictionary<string, int>();
                        Dictionary<string, int> midaMituDict = new Dictionary<string, int>();
                        Dictionary<string, decimal> kesSummaDict = new Dictionary<string, decimal>();
                        Dictionary<string, decimal> midaSummaDict = new Dictionary<string, decimal>();
                        foreach (var x in Kviitung.Kviitungid.Values)
                        {
                            // kuidas ma tagan, et kliendi rida on olemas (kuhu liita) 
                            // kui see klient on juba ette tulnud, siis on ta juba tabelis
                            // kui seda klienti veel pole olnud, lisame ta tabelisse
                            if (!kesMituDict.Keys.Contains(x.Klient)) // kui seda klienti veel ei ole
                            {
                                kesMituDict.Add(x.Klient, 0);         // siis lisame
                                kesSummaDict.Add(x.Klient, 0);         // siis lisame
                            }

                            if (!midaMituDict.Keys.Contains(x.Toode))
                            {
                                midaMituDict.Add(x.Toode, 0);
                                midaSummaDict.Add(x.Toode, 0);
                            }
                            // ja siis muudkui liidan - 
                            // leian klindile vastava rea, ja liidan talle uue koguse
                            kesMituDict[x.Klient] += x.Kogus;
                            kesSummaDict[x.Klient] += x.Summa;
                            midaMituDict[x.Toode] += x.Kogus;
                            midaSummaDict[x.Toode] += x.Summa;
                        }

                        // nüüd hakkan otsima, kellel on summa kõige suurem
                        // hakatuseks panen maha nulli (eeldus, et kogused on >0)
                        string mituKesNimi = "";
                        int mituKesKogus = 0;
                        string summaKesNimi = "";
                        decimal summaKesSumma = 0;

                        string mituMidaNimi = "";
                        int mituMidaKogus = 0;
                        string summaMidaNimi = "";
                        decimal summaMidaSumma = 0;

                        // siis käin läbi saadud tabeli ja vaatan
                        // kui kogus on suurem, kui see, mis mul meeles
                        // siis jätan meelde uue kogus ja selle omaniku
                        foreach(var x in kesMituDict)   // kesmitu on see "tabel"
                            if (x.Value > mituKesKogus)
                            {
                                mituKesNimi = x.Key;        // tabelis on võti (klient)
                                mituKesKogus = x.Value;     // ja väärtus (kogutud summa)
                            }
                        foreach (var x in kesSummaDict)   // kesmitu on see "tabel"
                            if (x.Value > summaKesSumma)
                            {
                                summaKesNimi = x.Key;        // tabelis on võti (klient)
                                summaKesSumma = x.Value;     // ja väärtus (kogutud summa)
                            }
                        foreach (var x in midaMituDict)   // kesmitu on see "tabel"
                            if (x.Value > mituKesKogus)
                            {
                                mituMidaNimi = x.Key;        // tabelis on võti (klient)
                                mituMidaKogus = x.Value;     // ja väärtus (kogutud summa)
                            }
                        foreach (var x in midaSummaDict)   // kesmitu on see "tabel"
                            if (x.Value > summaMidaSumma)
                            {
                                summaMidaNimi = x.Key;        // tabelis on võti (klient)
                                summaMidaSumma = x.Value;     // ja väärtus (kogutud summa)
                            }

                        // ja lõpuks trükin leitud tulemuse välja
                        Console.WriteLine($"kõige rohkem on ostnud {mituKesNimi} - {mituKesKogus} asja");
                        Console.WriteLine($"kõige rohkem on ostnud {summaKesNimi} - {summaKesSumma} raha eest");
                        Console.WriteLine($"kõige rohkem on müüdud {mituMidaNimi} - {mituMidaKogus} asja");
                        Console.WriteLine($"kõige rohkem on müüdud {summaMidaNimi} - {summaMidaSumma} raha eest");

                        //  proovi teha extensionitega
                        Console.WriteLine("\nsee on extensionitega\n");
                        Kviitung.Kviitungid.Values
                            .GroupBy(x => x.Klient, x => x.Kogus)  // rühmitame kliendi kaupa kogused
                            .Select(x => new { Klient = x.Key, Kogus = x.Sum() })  // seejärel leiame paarid - klient, koguste summa
                            .OrderByDescending(x => x.Kogus)  // sorteerima nad kahanevas järjekorras
                            .Take(1).ToList()   // võtame sealt esimese (ja teeme listiks - listil on olemas ForEach tehe
                            .ForEach(x => Console.WriteLine($"{x.Klient} ostis kõige rohkem {x.Kogus} asja")); // ja prindime ta välja

                        //  proovi teha LINQ avaldisega
                        Console.WriteLine("\nsee on LINQ avaldisega\n");

                        var q = from x in Kviitung.Kviitungid.Values
                                group x.Kogus by x.Klient into g
                                select new { g.Key, kogus = g.Sum() }
                                ;

                        foreach (var x in q.OrderByDescending(x => x.kogus).Take(1))
                            Console.WriteLine($"enim ostis {x.Key} koguse {x.kogus}"  );


                        // mõtle nüüd, kas ja kuidas teha sama asi ülejäänud kolme arvuga!
                        // summa kliendi kaupa
                        // kogus toote kaupa
                        // summa toote kaupa

                        Console.WriteLine("\ntükliga tehtud asjadest loeme tulemused extensionitega\n");

                        kesMituDict.OrderByDescending(x => x.Value).Take(1).ToList()
                            .ForEach(x => Console.WriteLine($"kõige rohkem asju ostis {x.Key} kokku {x.Value}"));

                        // umbes nii ma saaks asjad kätte

                        // veel 1 võimlaus
                        Console.WriteLine("\nproovime veel nii näiteks\n");

                        var q3 = Kviitung.Kviitungid.Values
                            .GroupBy(x => x.Klient)
                            .Select(x => new { Klient = x.Key, Kogused = x.Select(y => y.Kogus).Sum(), Raha = x.Select(y => y.Summa).Sum() });

                        q3.OrderByDescending(x => x.Kogused).Take(1).ToList()
                            .ForEach(x => Console.WriteLine($"kõige rohkem tükke ({x.Kogused}) ostis {x.Klient}"));

                        q3.OrderByDescending(x => x.Raha).Take(1).ToList()
                                                    .ForEach(x => Console.WriteLine($"kõige suurema summa ({x.Raha}) eest ostis {x.Klient}"));



                        // END 6
                        break;
                    case 7:
                        // harjutus 7 - samad kviitungid, mis eelmises ülesandes
                        // koosta nimekiri Klientide kaupa - nimi ja summa
                        // koosta nimekiri Toodete kaupa - nimetus, summa ja keskmine müügihind
                        // trüki need nimekirjad välja
                        // vaata kas toimib (ctrl-F5)
                        //
                        // TODO: 7. kirjuta seitsmenda harjutuse lahendus selle ja END 7 rea vahele
                        Console.WriteLine($"{harjutuseNumber}. harjutuse lahendus");


                        // END 7
                        break;
                    case 99:
                        // harjutus xx - 
                        // vaata kas toimib (ctrl-F5)
                        //
                        // TODO: xx. kirjuta xx harjutuse lahendus selle ja END xx rea vahele
                        Console.WriteLine($"{harjutuseNumber}. harjutuse lahendus");

                        // END xx
                        break;
                    default:
                        Console.WriteLine("Sellise numbriga harjutust meil ei ole");
                        break;
                }
            }
        }

        // valmis muutujad harjutuste tegemiseks 
        static int[] Arvud = { 7, -2, 18, 25, 6, 44, 28,0,  -9, -13, 12, -99 };
        static List<string> Nimed = new List<string> { "Henn", "Ants", "Peeter", "Joosep", "Kaarel" };
        static List<string> TeisedNimed = new List<string> { "Ants", "Kaarel", "Toomas" };
        static List<Inimene> Inimesed = new List<Inimene>
        {
            new Inimene {Nimi = "Test", Sünniaeg = DateTime.Parse("2000-2-29")},
            new Inimene {Nimi = "Henn", Sünniaeg = DateTime.Parse("1955-03-07")},
            new Inimene {Nimi = "Ants", Sünniaeg = DateTime.Parse("1977-04-01")},
            new Inimene {Nimi = "Peeter", Sünniaeg = DateTime.Parse("1968-12-08")},
            new Inimene {Nimi = "Jaak", Sünniaeg = DateTime.Parse("1999-01-18")},
            new Inimene {Nimi = "Malle", Sünniaeg = DateTime.Parse("2001-06-18")},
            new Inimene {Nimi = "Kalle", Sünniaeg = DateTime.Parse("2010-07-07")}


        };

        private static void Ettevalmistus(int harjutuseNumber)
        {
            switch (harjutuseNumber)
            {
                case 0:
                    break;

                case 6:
                case 7:
                    new Kviitung { Klient = "Henn", Toode = "Krokodill", Hind = 112, Kogus = 5 };
                    new Kviitung { Klient = "Ants", Toode = "Kummipall", Hind = 10, Kogus = 15 };
                    new Kviitung { Klient = "Peeter", Toode = "Krokodill", Hind = 100, Kogus = 2 };
                    new Kviitung { Klient = "Henn", Toode = "Jänes", Hind = 28, Kogus = 4 };
                    new Kviitung { Klient = "Jaak", Toode = "Auto", Hind = 200, Kogus = 1 };
                    new Kviitung { Klient = "Peeter", Toode = "Kummipall", Hind = 12, Kogus = 8 };
                    break;
            }
        }
    }

    // mõned lisaklassid harjutuste tegemiseks
    class Inimene
    {
        public string Nimi;
        public DateTime Sünniaeg;

        public int Vanus
        {
            get
            {
                int v = DateTime.Now.Year - Sünniaeg.Year;
                int l = (Sünniaeg.AddYears(v) >= DateTime.Now) ? -1 : 0;
                return v + l;
            } 
        }

        public override string ToString()
        {
            return $"{Nimi} on sündinud {Sünniaeg:dd.MMMM.yyyy} ja on {Vanus}-aastane"; 
        }
    }

    class Kviitung
    {
        public static Dictionary<int, Kviitung> Kviitungid = new Dictionary<int, Kviitung>();
        static int viimaneNr = 0;  

        int nr; // kviitungi number
        public string Klient; // kellele müüdi
        public string Toode; // mida müüdi
        public decimal Hind; // toote hind
        public int Kogus; // mitu müüdi
        public decimal Summa { get => Hind * Kogus; }

        public Kviitung()
        {
            nr = viimaneNr++;
            Kviitungid.Add(nr, this);
        }

        public override string ToString()
        {
            return $"{nr:00000} klient:{Klient} toode:{Toode} hind: {Hind} kogus: {Kogus} summa:{Summa}";
        }
    }
    
    class Koer
    {

    }
}
